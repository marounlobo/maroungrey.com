<?php get_header(); ?>

<div id="primary" <?php astra_primary_class(); ?>>

<section id='blogpage'>
        <h3 style='margin-bottom: 15px;'>Blog</h3>
        <p style='margin-bottom: 0;'>I continue to have keen interests in tech, web security, and development. My regular blog posts below contribute to the community, ensuring my lucrative knowledge on current trends and projects shares with other enthusiasts.</p>
</section>


<?php if ( astra_page_layout() == 'left-sidebar' ) : ?> 

<?php get_sidebar(); ?>

<?php endif ?>
<div id="primary" <?php astra_primary_class(); ?>>
    <?php 
    astra_primary_content_top();
    
    astra_content_loop();

    astra_pagination();

    astra_primary_content_bottom(); 
    ?>
</div><!-- #primary -->
<?php 
if ( astra_page_layout() == 'right-sidebar' ) :

get_sidebar();

endif;

?>
<!-- search box start -->
<?php
get_search_form( $echo );
?>
<!-- search box end -->

<?php get_footer(); ?>

<style>
    #blogpage {
        max-width: 500px;
        margin: 50px auto;
        text-align: center;
    }
</style>