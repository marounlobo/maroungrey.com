<?php
/**
 * The header for Astra Theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?><!DOCTYPE html>
<?php astra_html_before(); ?>
<html <?php language_attributes(); ?>>
<head>
<?php astra_head_top(); ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11">



<!-- Custom start -->

	<!-- fontawesome -->
	<script src="https://kit.fontawesome.com/2628de7551.js" crossorigin="anonymous"></script>	
    <!-- Gill Sans Bold font -->
    <style type="text/css">
        @import url("https://www.fast.fonts.net/lt/1.css?apiType=css&c=032714e3-a3bf-4340-81c2-837f04c339bf&fontids=5727203");
        @font-face{
            font-family:"Gill Sans MT W05 ExtraBold";
            src:url("https://maroungrey.com/wp-content/themes/astra/fonts/5727203/3033fde8-35fe-4b32-a6c8-b9ddf2d40d2b.woff2") format("woff2"),url("https://maroungrey.com/wp-content/themes/astra/fonts/5727203/3cf2442c-f5d8-4f8f-b3d9-ad4cdb2b1fc2.woff") format("woff");
        }
    </style>  
<!-- Custom end -->


<?php wp_head(); ?>
<?php astra_head_bottom(); ?>
</head>

<body <?php astra_schema_body(); ?> <?php body_class(); ?>>
<?php astra_body_top(); ?>
<?php wp_body_open(); ?>
<div 
<?php
	echo astra_attr(
		'site',
		array(
			'id'    => 'page',
			'class' => 'hfeed site',
		)
	);
	?>
>
	<a class="skip-link screen-reader-text" href="#content"><?php echo esc_html( astra_default_strings( 'string-header-skip-link', false ) ); ?></a>
	<?php 
	astra_header_before(); 

	astra_header(); 

	astra_header_after();

	astra_content_before(); 
	?>



	<div id="content" class="site-content">
		<div class="ast-container">
		<?php astra_content_top(); ?>
