<div id="name-animation">
      <div id="name-container">
          <h1 class="fName">Maroun  </h1>
          <h1 class="mName">Grey</h1>
      </div>
      <p id="myHeadline">Software and Web Development professional</p>
      <div class="custom-icons">
          <a class="custom-icon" href="https://github.com/El-lobo-cimarron" target="_blank"><i class="fab fa-github fa-2x"></i></a>
          <a class="custom-icon" href="https://www.linkedin.com/in/maroun-barqawi-a53b11180/" target="_blank"><i class="fab fa-linkedin fa-2x"></i></a>
          <a class="custom-icon" href="mailto:maroun.barqawi@gmail.com" target="_blank"><i class="fas fa-envelope fa-2x"></i></a>
          <a class="custom-icon" href="https://codesandbox.io/u/El-lobo-cimarron" target="_blank"><i class="fab fa-codepen fa-2x"></i></a>
      </div>
  </div>

<style>
.custom-block {
    position: absolute;
    margin: auto;
    right: 0;
    left: 0;
    top:30%;
}
div#name-container {
    width: fit-content;
    margin: auto;
}
a.custom-icon {
    padding: 0 10px;
    color: #222
}
.custom-icon:hover {
  color:#444;
}
h1.fName, h1.mName {
  font-family:"Gill Sans MT W05 ExtraBold";
  font-size: 4vw;
  width:fit-content;
  display: inline-block;
  padding: 0 10px;
  text-transform: uppercase;
}
.fName {
    background-image: -webkit-linear-gradient(left, #222 25%, #5A1010 37.5%, #800000, #5A1010 62.5%, #222 75%);
    background-image: linear-gradient(135deg, #222 25%, #5A1010 37.5%, #800000, #5A1010 62.5%, #222 75%);
    background-position: right center;
    background-size: 500% auto;
    -webkit-background-clip: text;
    background-clip: text;
    -webkit-text-fill-color: transparent;
    text-fill-color: transparent;
    transition: color 200ms linear;
  }
.fName-hover {
    background-position: left center;
    transition: background-position 1500ms ease-out;
  }
.mName {
    background-image: -webkit-linear-gradient(left, #222 25%, #333 37.5%, #444, #333 62.5%, #222 75%);
    background-image: linear-gradient(135deg, #222 25%, #333 37.5%, #444, #333 62.5%, #222 75%);
    background-position: right center;
    background-size: 500% auto;
    -webkit-background-clip: text;
    background-clip: text;
    -webkit-text-fill-color: transparent;
    text-fill-color: transparent;
    transition: color 200ms linear;
  }
.mName-hover {
    background-position: left center;
    transition: background-position 1500ms ease-out;
  }
.fa-2x {
    font-size: 1.3em;
}
.fa-2x:hover {
  transform: scale(1.1);
}
/* full width banner */
 @media (min-width: 544px){
.ast-container {
    max-width: 100%;
    padding: 0;
    }
img.attachment-large.size-large.wp-post-image {
    width: 100%;
    }
.page .has-post-thumbnail .post-thumb img {
    margin-bottom: 0;
}
.page .entry-header {
    margin-bottom: 0;
}
}
/* positioning of name animation */
#name-animation {
    position: absolute;
    margin: auto;
    left: 0;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
    }
  #myHeadline {
    color: #222;
    margin: 0 auto 3vw;
    width: fit-content;
    font-size: 1em;
    transform: translateY(60%);
  }
  .custom-icons {
    width: fit-content;
    margin: auto;
  }
@media screen and (min-width:751px) and (max-width:950px) {
  #myHeadline {
    font-size:12px;
    margin: -5px auto 3vw;
  }
}  
@media screen and (min-width:751px) and (max-width:1150px) {
  #myHeadline {
    margin: -10px auto 3vw;
  }
} 
@media screen and (max-width:750px){
#name-animation {
    display:none;
    }
}
</style> 



<!-- JS -->

<script src="https://code.jquery.com/jquery-3.1.0.js">
  https: //code.jquery.com/jquery-3.1.0.js
</script>

<script>

$(document).ready(function(){

    //myName animation
    $("#name-container").mouseenter(function(e){
       $(".fName").addClass("fName-hover");
       $(".fName").html("Maroon");
     }).mouseleave(function(e){
         $(".fName").removeClass("fName-hover");
         $(".fName").html("Maroun");
     });
    $("#name-container").mouseenter(function(e){
      $(".mName").addClass("mName-hover");
    }).mouseleave(function(e){
      $(".mName").removeClass("mName-hover");
    });

});

</script>
