<div id="prttitle" style="text-align:center; max-width:500px; margin: auto;">
    <h3 class="portfolio-title">Recent Projects</h3>
</div>
<div id="portfolio-block">
    <div class='portfolio-container' id="portfolio-1">
        <img class="portfolio-image" src="https://maroungrey.com/wp-content/uploads/2021/04/panoplie-card-1.png" alt="">
        <div class="portfolio-middle">
            <div class="portfolio-vertical">
                <p class="portfolio-description">Panoplie is a custom Magento 2 website I built, featuring high functioning dropdowns with inventory checking, auto cart updating, and checkout customization.</p>
                <a href="https://www.panoplie.com" target="_blank">
                    <div class="portfolio-text">Link</div>
                </a>
            </div>
        </div>
    </div>

    <div class='portfolio-container' id="portfolio-2">
        <img class="portfolio-image" src="https://maroungrey.com/wp-content/uploads/2021/04/bswish-card.png" alt="">
        <div class="portfolio-middle">
            <div class="portfolio-vertical">
                <p class="portfolio-description">B Swish is a byproduct of my Magento migration capabilities. The website was transferred from Magento 1 to 2, adding elastic search, optimizing page speeds, and layout customizations.</p>
                <a href="https://www.bswish.com" target="_blank">
                    <div class="portfolio-text">Link</div>
                </a>
            </div>
        </div>
    </div>

    <div class='portfolio-container' id="portfolio-3">
        <img class="portfolio-image" src="https://maroungrey.com/wp-content/uploads/2021/04/bison-bullion-1.png" alt="">
        <div class="portfolio-middle">
            <div class="portfolio-vertical">
                <p class="portfolio-description">Bison Buillon is a Magento 2 website constructed with the Luma theme. I developed a custom extension for the site that updates pricing against the current market costs of gold and silver. </p>
                <a href="https://bisonbullion.com" target="_blank">
                    <div class="portfolio-text">Link</div>
                </a>
            </div>
        </div>
    </div>
</div>


<style>
    div#portfolio-block {
        display: flex;
        justify-content: center;
        padding-bottom: 15px;
        margin-top: 30px;
    }

    .portfolio-container {
        position: relative;
        max-width: 300px;
        display: inline-block;
        margin: 0 12px;
        vertical-align: top;
    }

    .portfolio-image {
        opacity: 1;
        display: block;
        width: 100%;
        height: auto;
        transition: .5s ease;
        backface-visibility: hidden;
    }

    .portfolio-vertical {
        margin: 0;
        position: absolute;
        top: 80%;
        padding: 5% 5% 0;
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
    }

    .portfolio-middle {
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 0;
        width: 100%;
        opacity: 0;
        transition: .5s ease;
        background-color: #fff;
    }

    .portfolio-hover {
        height: 100px;
    }

    .portfolio-container:hover .portfolio-middle {
        opacity: 1;
    }

    p.portfolio-description {
        width: 100%;
        margin: auto;
        text-align: left;
        color: #222;
        font-size: 12px;
    }

    .portfolio-text {
        float: right;
        font-size: 12px;
        line-height: 10px;
        margin-top: -13px;
    }

    .portfolio-title,
    .portfolio-description {
        text-align: center;
    }

    .portfolio-title {
        margin-bottom: 15px;
    }

    @media screen and (max-width:600px) {
        .portfolio-description-main {
            margin-bottom: 10px;
        }
    }

    @media screen and (max-width:750px) {
        section#myPortfolio {
            padding: 0;
        }
        .main-content {
            padding-left: 0;
            padding-right: 0;
        }
        #prttitle {
            padding: 30px 45px 0;
        }
        .portfolio-container {
            background: white;
            max-width: unset;
            padding-bottom: 15px;
            margin: 10px auto 15px;
        }
        #blockPortfolio {
            margin: auto;
            max-width: 500px;
        }
        .portfolio-vertical {
            position: relative;
            transform: none;
            padding: 0 15px 0;
        }

        #portfolio-block {
            padding-bottom: 15px;
        }

        div#portfolio-block {
            display: block;
            margin-top: 0;
            padding: 0 45px 30px;
            max-width: 500px;
        }
        .portfolio-middle {
            position: relative;
            opacity: 1;
            background-color: transparent;
        }
        p.portfolio-description {
            width: 100%;
            color: black;
            margin-top: 15px;
        }
        .portfolio-hover {
            height: 0;
        }
        .portfolio-description-main {
            text-align: left;
            padding: 0px;
            font-size: 12px;
        }
        .portfolio-title {
            text-align: left;
            padding: 0px;
            margin-bottom: 10px;
        }
        div#portfolio-block {
            margin-top: 0;
        }
    }
</style>

<script>
    jQuery(document).ready(function($) {
        $('#portfolio-1').hover(function() {
                $('.portfolio-middle').addClass('portfolio-hover');
            },
            function() {
                $('.portfolio-middle').removeClass('portfolio-hover');
            });
        $('#portfolio-2').hover(function() {
                $('.portfolio-middle').addClass('portfolio-hover');
            },
            function() {
                $('.portfolio-middle').removeClass('portfolio-hover');
            });
        $('#portfolio-3').hover(function() {
                $('.portfolio-middle').addClass('portfolio-hover');
            },
            function() {
                $('.portfolio-middle').removeClass('portfolio-hover');
            });
    });
</script>