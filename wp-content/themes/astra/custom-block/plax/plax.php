<script type="text/javascript" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-master/js/jquery.min.js"></script>
<script type="text/javascript" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-master/js/plax.js"></script>



<img class="plax-image" id="img-1" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/1.png">
<img class="plax-image" id="img-2" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/2.png">
<img class="plax-image" id="img-3" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/3.png">
<img class="plax-image" id="img-4" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/4.png">
<img class="plax-image" id="img-5" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/5.png">
<img class="plax-image" id="img-6" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/6.png">
<img class="plax-image" id="img-7" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/7.png">
<img class="plax-image" id="img-8" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/8.png">
<img class="plax-image" id="img-9" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/9.png">
<img class="plax-image" id="img-10" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/10.png">
<img class="plax-image" id="img-11" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/11.png">
<img class="plax-image" id="img-12" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/12.png">
<img class="plax-image" id="img-13" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/13.png">
<img class="plax-image" id="img-14" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/14.png">
<img class="plax-image" id="img-15" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/15.png">
<img class="plax-image" id="img-16" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/16.png">
<img class="plax-image" id="img-17" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/17.png">
<img class="plax-image" id="img-18" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/18.png">
<img class="plax-image" id="img-19" src="https://maroungrey.com/wp-content/themes/astra/custom-block/plax/plax-images/19.png">


<style>
.plax-image {
    height: 10vh;
    position: absolute;
}
#img-1 { /*ubuntu*/ 
    bottom:10%;
    left:23%;
}
#img-2 { /*git*/
    bottom:15%;
    right:23%;
}
#img-3 { /*css*/
    height: 15vh;
    top:25%;
    left:20%;
}
#img-4 { /*react*/
    height: 17vh;
    top:33%;
    right:23%;
}
#img-5 { /*js*/
    top:29%;
    left:15%;
}
#img-6 { /*wordpress*/
    top:25%;
    right:20%;
}
#img-7 { /*html*/
    top:55%;
    left:15%;
}
#img-8 { /*sql*/
    bottom:25%;
    right:17%;
}
#img-9 { /*github*/
    top:30%;
    right:10%;
}
#img-10 { /*unity*/
    height: 15vh;
    top:40%;
    left:7%;
}
#img-11 { /*stack overflow*/
    top:20%;
    left:5%;
}
#img-12 { /*python*/
    height:7vh;
    top:10%;
    left:10%;
}
#img-13 { /*php*/
    height:7vh;
    top:15%;
    right:5%;
}
#img-14 { /*mongodb*/
    top:10%;
    left:17%;
}
#img-15 { /*magento*/
    top:10%;
    right:15%;
}
#img-16 { /*BitBucket*/
    height: 15vh;
    bottom:5%;
    right:7%;
}
#img-17 { /*C#*/
    height:9vh;
    top:10%;
    right:25%;
}
#img-18 { /*node*/
    bottom:10%;
    left:2%;
}
#img-19 { /*visual studio*/
    bottom:35%;
    right:2%;
}
</style>

<script>
jQuery(function ($) {

$('#img-1').plaxify({"xRange":15,"yRange":15,"invert":true})
$('#img-2').plaxify({"xRange":15,"yRange":15})
$('#img-3').plaxify({"xRange":5,"yRange":5,"invert":true})
$('#img-4').plaxify({"xRange":5,"yRange":15,"invert":true})
$('#img-5').plaxify({"xRange":5,"yRange":15})
$('#img-6').plaxify({"xRange":10,"yRange":10})
$('#img-7').plaxify({"xRange":20,"yRange":5})
$('#img-8').plaxify({"xRange":20,"yRange":5,"invert":true})
$('#img-9').plaxify({"xRange":5,"yRange":15})
$('#img-10').plaxify({"xRange":5,"yRange":5,"invert":true})
$('#img-11').plaxify({"xRange":10,"yRange":10})
$('#img-12').plaxify({"xRange":5,"yRange":20,"invert":true})
$('#img-13').plaxify({"xRange":20,"yRange":5})
$('#img-14').plaxify({"xRange":20,"yRange":5,"invert":true})
$('#img-15').plaxify({"xRange":5,"yRange":5})
$('#img-16').plaxify({"xRange":5,"yRange":5})
$('#img-17').plaxify({"xRange":5,"yRange":15,"invert":true})
$('#img-18').plaxify({"xRange":10,"yRange":10,"invert":true})
$('#img-19').plaxify({"xRange":15,"yRange":15,"invert":true})

$.plax.enable({ "activityTarget": $('#cstmbg')})

})
</script>