<?php get_header(); ?>

<div id="primary" <?php astra_primary_class(); ?>>

<!-- custom block start -->
<?php include 'custom-block/picture.php' ?>



<div id='myMainContent'>

    <section id='myAboutMe'>
        <h3 style='margin-bottom: 15px;'>About Me</h3>
        <p style='margin-bottom: 20px;'>I am a full stack web developer who has worked with multiple start-ups with nearly 2 years of experience. My skills range from back-end development and server management to front-end and web design. I also write a blog where I share my latest technology and security research. Please see my work below. </p>
        <a href="mailto:maroun.barqawi@gmail.com" target="_blank">
            <button style="font-size: 12px; padding: 10px 20px; border-radius: 5px;">Contact Me</button>
        </a>
    </section>
    <section id='myPortfolio' style='background:#c7c7c7;'>
        <div id="blockPortfolio"><?php include 'custom-block/block-portfolio.php'; ?></div>
    </section>
    <section id='myBlog'>
    <h3 id="myBlogTitle">Latest Blog</h3>
        <div id="blog-block">
        <?php
        $args = array( 'numberposts' => 3, 'order'=> 'ASC', 'orderby' => 'title' );
        $postslist = get_posts( $args );
        foreach ($postslist as $post) :  setup_postdata($post); ?> 
            <div class='myPost'>
                <?php the_post_thumbnail(); ?>
                <br />
                <div style="padding:10px;">
                    <p style="font-size:10px; margin: 0;"><?php the_date(); ?></p>
                    <?php the_title(); ?>   
                    <?php the_excerpt(); ?>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    <a href="/blog" id='blog-button'>
        <button style='font-size: 12px; padding: 10px 20px; border-radius: 5px;'>View More</button>
    </a>
    </section>
</div>

<style>
    section{
        padding: 80px 0;
    }
    p {
    margin-bottom: 0;
    }
    .site-header {
    position: fixed;
    width: 100%;
    }
    
    #myAboutMe {
        max-width: 500px;
        margin: auto;
        text-align: center;
    }
    #blockPortfolio {
        margin: auto;
    }
    #myBlog {
        width: fit-content;
        margin: auto;
        padding: 80px 0 40px;
    }
    .myPost {
        max-width: 300px;
        display: inline-block;
        margin: 0 10px;
        vertical-align: top;
        border: 1px solid #ccc;
    }
    #blog-button {
        display: block;
        margin: 50px auto 10px;
        width: fit-content;
    }
    #myBlogTitle{
        margin-bottom: 40px;
        text-align:center;
    }
    .ast-separate-container #primary {
    padding: 0;
    }
    #blog-block {
        display: flex;
        justify-content: center;
    }
    @media only screen and (max-width: 750px) {
    #myBlog {
    padding: 30px 45px;
    max-width: 500px;
    }
    #myAboutMe {
    text-align: left;
    font-size: 12px;
    padding: 30px 45px;
    }
    .myPost {
    margin: 0 0 25px;
    max-width: unset;
    }
    #blog-block {
    display: block;
    }
    #blog-button {
        margin: 0 auto;
    }
    #myBlogTitle{
        margin-bottom: 25px;
    }
    .ast-icon svg {
    top: 3px;
    }
    }
    @media (max-width: 544px) {
    .ast-separate-container #content .ast-container {
    padding-left: 0;
    padding-right: 0;
    }
}
</style>



<?php get_footer(); ?>