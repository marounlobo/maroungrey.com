<h3 style="text-align:center; padding: 5px; font-size:20px; font-weight:600">My Recent Projects</h3>
<p style="text-align: center;">I'm currently focused on Magento and Wordpress development.</p>

<div id="portfolio-block" style="margin-top: 30px;"> 
    <div class='portfolio-container'>
        <img class="portfolio-image" src="https://maroungrey.com/wp-content/uploads/2021/04/panoplie-card-1.png" alt="">
        <div class="portfolio-middle">
            <div class="portfolio-vertical">
                <p class="portfolio-description">Built Magento 2 website, developed custom dropdown that checks inventory and autoupdating cart, checkout customizations.</p>
                <a href="https://www.panoplie.com" target="_blank"><div class="portfolio-text">Link</div></a>
            </div>
        </div>
    </div>

    <div class='portfolio-container'>
        <img class="portfolio-image" src="https://maroungrey.com/wp-content/uploads/2021/04/bswish-card.png" alt="">
        <div class="portfolio-middle">
            <div class="portfolio-vertical">
                <p class="portfolio-description">Migrated Magento 1 website to Magento 2, insalled Elasticsearch, optimized speed and SEO, added layout customizations.</p>
                <a href="https://www.bswish.com" target="_blank"><div class="portfolio-text">Link</div></a>
            </div>
        </div>
    </div>

    <div class='portfolio-container'>
        <img class="portfolio-image" src="https://maroungrey.com/wp-content/uploads/2021/04/bison-bullion-1.png" alt="">
        <div class="portfolio-middle">
            <div class="portfolio-vertical">
                <p class="portfolio-description">Built Magento 2 website with Luma theme, developed extention that update prices based on the current gold/silver price. </p>
                <a href="https://bisonbullion.com" target="_blank"><div class="portfolio-text">Link</div></a>
            </div>
        </div>
    </div>
</div>


<style>
div#portfolio-block {
    display: flex;
    justify-content: center;
    padding-bottom: 15px;
}
.portfolio-container {
  position: relative;
  width: 30%;
  margin:10px;
}

.portfolio-image {
  border-radius: 15px;
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}
.portfolio-vertical{
  margin: 0;
  position: absolute;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}
.portfolio-middle {
  border-radius: 15px;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  opacity: 0;
  transition: .5s ease;
  background-color: #000;
}
.portfolio-container:hover .portfolio-middle {
  opacity: 0.9;
}
p.portfolio-description {
    width: 80%;
    margin: auto;
    text-align: center;
    color: white;
    font-size: 15px;
}
.portfolio-text {
  color: white;
  font-size: 16px;
  padding: 0 32px;
  width: fit-content;
  border: 2px solid black;
  border-color:#660508;
  background-color: transparent;
  cursor: pointer;
  border-radius:15px;
  margin: 10px auto 0;
}
.portfolio-text:hover {
  background-color: #660508;
}


@media screen and (max-width:1040px){
    .main-content {
        padding-left:0;
        padding-right:0;
    }
    .portfolio-vertical{
        position: relative;
        transform: none;
    }
    #portfolio-block{
        padding-bottom: 15px;
    }
    div#portfolio-block {
    display: block;
    }
    .portfolio-container {
        position: relative;
        width: auto;
        margin: 15px 15px 30px;
    }
    .portfolio-middle {
        position: relative;
        opacity: 1;
        background-color: transparent;
    }
    p.portfolio-description {
        width: 100%;
        color: black;
        margin-top: 15px;
        font-size: 15px;
    }
    .portfolio-text {
        color: white;
        font-size: 15px;
        padding: 0px 32px;
        width: fit-content;
        border: none;
        border-color: transparent;
        background-color: #660508;
        border-radius: 15px;
        margin: 5px auto 0;
    }
}

</style>

