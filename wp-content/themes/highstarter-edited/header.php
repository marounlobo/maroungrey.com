<?php
/**
 * The header for the Highstarter theme
 *
 * This is the template that displays all of the <header> section
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Highstarter
 * 
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
    <meta charset="<?php bloginfo('charset');?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- Gill Sans Bold font -->
    <style type="text/css">
        @import url("https://www.fast.fonts.net/lt/1.css?apiType=css&c=032714e3-a3bf-4340-81c2-837f04c339bf&fontids=5727203");
        @font-face{
            font-family:"Gill Sans MT W05 ExtraBold";
            src:url("https://maroungrey.com/wp-content/themes/highstarter/fonts/5727203/3033fde8-35fe-4b32-a6c8-b9ddf2d40d2b.woff2") format("woff2"),url("https://maroungrey.com/wp-content/themes/highstarter/fonts/5727203/3cf2442c-f5d8-4f8f-b3d9-ad4cdb2b1fc2.woff") format("woff");
        }
    </style>  
    <!-- Montserrat -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;400;600&display=swap" rel="stylesheet">  
    <!-- fontawesome -->
    <script src="https://kit.fontawesome.com/2628de7551.js" crossorigin="anonymous"></script>
<?php wp_head();?>
</head>
<body <?php body_class();?>>
<?php wp_body_open(); ?>
    <header class="site-header">
        <a class="screen-reader-text skip-link" href="#content"><?php _e( 'Skip to content', 'highstarter'); ?></a>
        <div class="site-header-wrapper">
            <div class="site-logo-wrapper">
            <?php // Insert logo through WP admin here
               highstarter_the_custom_logo(); ?>
            </div>
            <div class="main-navigation-container">
                <?php get_template_part('template-parts/header/navigation');?>
            </div>
        </div><!-- .header wrapper -->
        <?php get_template_part('template-parts/header/site-branding');?>
    </header><!-- .site-header -->
    <main class="site-content">
        <div class="wrapper">