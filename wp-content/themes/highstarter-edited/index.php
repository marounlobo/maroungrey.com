<?php get_header(); ?>
<!-- Start of About Me content -->

<section id="aboutme">
    <div class="container" style="background-color:#fff; padding: 65px 35px; max-width: 100%;">
        <div class="row" style="margin:auto;">
            <div class="custom-about-me" style="max-width: 600px; margin: auto;">
            <h2 style="text-align: center; font-weight: 600; font-size: 35px; color: #222222;">Hi, I'm Maroun.</h2>
                <h3 style="text-align: center; color: #222; font-size: 20px;">Almost like a color!</h3>
                <p style="
                        text-align: center;
                        color: #222222;
                        margin: auto;
                        margin-top: 15px;
                        line-height: 20px;
                        ">I'm a self-taught full-stack software engineer. My goal is to push technology to do things better, faster and easier. I learn by doing. I'm quietly confident, naturally curious, and motivated to constantly improving my skills.</p>
            </div>
        </div>
    </div>
</section>

<!-- Portfolio -->

<section id='portfolio' style="padding: 60px 0; background-color: #f0f0f0;">
    <?php include 'blocks/block-portfolio.php'; ?>
</section>

<!-- Blog -->
<section id="blog">
    <h3>Blog</h3>
    [bdp_post_carousel design="design-2" show_date="false" show_author="false" show_comments="false" show_content="true"]
</section>



<!-- Start of main-content -->
<section id="content" class="site-section" style="padding:0;">
    <div class="container" style="background-color: #f0f0f0;">
    <h3 style="text-align: center; color: #222; font-size: 20px; font-weight: 600; padding: 20px 0 0;">Latest Blog</h3>
        <div class="row">
            <div class="post-title">
                <h3><?php _e('', 'highstarter'); ?></h3>
            </div>
        </div>
        <div class="row blog-entries">
            <div class="main-content">
                <div class="row">
                    <?php
                    //Dynamic content here
                    if (have_posts()) :
                        while (have_posts()) : the_post();
                            get_template_part('template-parts/post/content', 'excerpt', get_post_format());
                        endwhile;
                        highstarter_numeric_posts_nav();
                    else :
                        _e('There are no posts!', 'highstarter');
                    endif;
                    ?>
                </div>
            </div>
            <!-- END of main-content -->
            <!-- Show Sidebar -->
            <?php get_sidebar() ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>